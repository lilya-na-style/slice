package test.FirstTask;

import main.FirstTask.ArraySortMethod;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ArraySortMethodTest {


    ArraySortMethod cut = new ArraySortMethod();

    static Arguments[] sortArrayTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{2, 1, 3, 4, 6}, new int[]{6, 4, 3, 1, 2}),
                Arguments.arguments(new int[]{9, 8, 6, 5, 4}, new int[]{4, 5, 6, 8, 9}),
        };
    }

    @ParameterizedTest
    @MethodSource("sortArrayTestArgs")
    void swap(int[] expected, int[] array) {
        int[] actual = cut.sortArray(array);
        Assertions.assertArrayEquals(expected, actual);
    }
}