package test.FourthTask;

import main.FourthTask.models.Train;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class TrainTest {


    @Test
    void toStringTest(){
        Train cut = new Train("Kyiv", "123K", 7.30, 50);
        String  actual = cut.toString();
        String expected = "Train{destination='Kharkiv', numberOfTrain='228KH', time=9.9, placesNumber=42}";
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] equalsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, new Train("Kharkiv", "228KH", 9.00, 42), new Train("Kharkiv", "228KH", 9.00, 42)),
                Arguments.arguments(false, new Train("Harkov", "128h", 8.30, 80), new Train("Harkov", "128h", 7.30, 80)),
                Arguments.arguments(false, new Train("Khark0v", "860K", 7.30, 50), new Train("Kharkov", "789Kh", 7.30, 60)),

        };
    }

    @ParameterizedTest
    @MethodSource("equalsTestArgs")
    void equalsTest(boolean expected, Train train1, Train train2){
        boolean actual = train1.equals(train2);
        Assertions.assertEquals(expected, actual);
    }

}