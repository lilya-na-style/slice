package test.FifthTask;

import main.FifthTask.MathServicesFactory;
import main.FifthTask.caches.MathFactoryCache;
import main.FifthTask.implementations.Division;
import main.FifthTask.implementations.Minus;
import main.FifthTask.implementations.Multiply;
import main.FifthTask.implementations.Plus;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class MathFactoryCacheTest {

    Plus mathPlus = new Plus();
    Minus mathMinus = new Minus();
    Multiply mathMultiply = new Multiply();
    Division mathDivision = new Division();

    MathServicesFactory[] mathFactory = new MathServicesFactory[]{mathPlus, mathMinus, mathMultiply, mathDivision};

    MathFactoryCache cut = new MathFactoryCache(mathFactory);


    static Arguments[] mathFactoryCacheTestArgs() {
        return new Arguments[]{

                Arguments.arguments(9, "+", 6, 3),
                Arguments.arguments(3, "-", 9, 6),
                Arguments.arguments(12, "*", 6, 2),
                Arguments.arguments(5, "/", 10, 2),

        };
    }

    @ParameterizedTest
    @MethodSource("mathFactoryCacheTestArgs")

    void mathFactoryCacheTest(double expected, String operation, double var1, double var2) {
        double actual = cut.getFactory(operation, var1, var2);
        Assertions.assertEquals(expected, actual);
    }
}