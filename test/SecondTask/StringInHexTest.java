package test.SecondTask;

import main.SecondTask.StringInHex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class StringInHexTest {

    StringInHex cut = new StringInHex();

    static Arguments[] isHexNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, "01AB"),
                Arguments.arguments(true, "2B74"),
                Arguments.arguments(false, "228p"),
                Arguments.arguments(true, "fF"),
                Arguments.arguments(true, "aB1"),
                Arguments.arguments(true, "0F9")


        };
    }

    @ParameterizedTest
    @MethodSource("isHexNumberTestArgs")
    void RegexHexNumberTest(boolean expected, String hexNumber){
        boolean actual = cut.isHexNumber(hexNumber);
        Assertions.assertEquals(expected, actual);
    }
}
