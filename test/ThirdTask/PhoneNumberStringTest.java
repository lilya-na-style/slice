package test.ThirdTask;

import main.ThirdTask.PhoneNumberString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class PhoneNumberStringTest {

    PhoneNumberString cut = new PhoneNumberString();


    static Arguments[] isStringPhoneNumberTestArgs(){

        return new Arguments[]{

                Arguments.arguments("+380 97 601-70-71", true),
                Arguments.arguments("+3 789 123-34-22", true),
                Arguments.arguments("+3 333 7677-4-22", false),
                Arguments.arguments("+1 212 555-45-6A", false),
                Arguments.arguments("+096 333 222-00-00", false),
                Arguments.arguments("+1 333 55-004-5A", false),
        };
    }


    @ParameterizedTest
    @MethodSource("isStringPhoneNumberTestArgs")

    void getNumbTest(String s, boolean expected) {
        boolean actual = cut.isStringPhoneNumber(s);
        Assertions.assertEquals(expected, actual);
    }
}
