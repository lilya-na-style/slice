package main.FourthTask.services;

import main.FourthTask.models.Train;

import java.util.Comparator;

public class ComparatorForTrain implements Comparator<Train> {


    @Override
    public int compare(Train o1, Train o2) {

        int result = o1.getPlacesNumber()-o2.getPlacesNumber();

        if(result == 0){
            result = o1.getDestination().compareTo(o2.getDestination());
            if(result==0){

                result = (int) ((o1.getTime()-o2.getTime())*100);
            }
        }

        return result;
    }
}