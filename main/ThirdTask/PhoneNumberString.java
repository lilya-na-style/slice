package main.ThirdTask;

import java.util.regex.Pattern;

public class PhoneNumberString {

    public static boolean isStringPhoneNumber(String s){

        String number = "\\+[1-9]\\d{0,2} \\d{2,3} \\d{3}\\-\\d{2}\\-\\d{2}";

        if (Pattern.compile(number).matcher(s).matches()) {
            return true;
        }
        return false;
    }
}
