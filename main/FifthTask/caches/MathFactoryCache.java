package main.FifthTask.caches;

import main.FifthTask.MathServicesFactory;

public class MathFactoryCache {

    private MathServicesFactory[] mathFactories;



    public MathFactoryCache(MathServicesFactory[] mathFactories) {
        this.mathFactories = mathFactories;
    }

    public double getFactory(String operation, double var1, double var2){

        switch (operation)
        {
            case "+":
                return mathFactories[0].countTwoValue(var1, var2);
            case "-":
                return mathFactories[1].countTwoValue(var1, var2);
            case "*":
                return mathFactories[2].countTwoValue(var1, var2);
            case "/":
                return mathFactories[3].countTwoValue(var1, var2);
            default:
                System.out.println("Invalid command");
                return 0;

        }
    }
}
