package main.FifthTask.implementations;

import main.FifthTask.MathServicesFactory;

public class Division implements MathServicesFactory {

    @Override
    public double countTwoValue(double var1, double var2) {
        double result = (var1/var2)*1000_000;
        result = Math.round(result);
        result = result/1000_000;
        return result;
    }
}
