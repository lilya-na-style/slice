package main.FifthTask.implementations;

import main.FifthTask.MathServicesFactory;

public class Plus implements MathServicesFactory {
    @Override
    public double countTwoValue(double var1, double var2) {
        return var1+var2;
    }
}
