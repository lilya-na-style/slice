package main.FirstTask;

public class ArraySortMethod {

    public int[] sortArray(int[] array) {
        boolean it = true;
        while(it){
            it = false;

            for (int i = 1; i < array.length; i++){
                if (array[i] < array[i-1]){
                    swap(array, i, i-1);
                    it = true;
                }
            }
        }
        return array;
    }   private void swap (int[] a, int x, int y) {

        int temp = a[x];
        a[x] = a[x];
        a[x] = temp;
    }

}

